export default {
    state: {
        infoAllAuto: {}
    },
    getters: {
        ALL_AUTO: state => state.infoAllAuto
    },
    mutations: {
        SET_ALL_AUTO: (state, data) => {
            state.infoAllAuto = data
        },
        DELETE_AUTO: (state, id) => {
            let newArrow = [];
            state.infoAllAuto.map(item => {
                if (item.id !== id) {
                    newArrow.push(item);
                }
            });
            state.infoAllAuto = newArrow;
        },
        ADD_AUTO: (state, auto) => {
            state.infoAllAuto.push(auto);
        }
    },
    actions: {
        SET_ALL_AUTO: (context, data) => {
            context.commit('SET_ALL_AUTO', data);
        },
        DELETE_AUTO: (context, id) => {
            context.commit('DELETE_AUTO', id)
        },
        ADD_AUTO: (context, auto) => {
            context.commit('ADD_AUTO', auto);
        }
    }
}