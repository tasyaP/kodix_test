import Vue from 'vue';
import Vuex from 'vuex';
import auto from "@/store/modules/auto";

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        auto
    },
    strict: process.env.NODE_ENV !== 'production'
});

export default store;