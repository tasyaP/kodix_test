import Vue from 'vue'
import App from './App.vue'
import store from "@/store";
import axios from "axios";
import vSelect from 'vue-select';
import OpenIndicator from "@/components/OpenIndicator";
import mask from 'v-mask';

Vue.config.productionTip = false;
Vue.component('v-select', vSelect);
vSelect.props.components.default = () => ({OpenIndicator}); /*Замена иконки по умолчанию, на нужную из макета в селекте*/
Vue.use(mask);

/*users directives*/
Vue.directive('labelStyle', {
  inserted: function (el) {
    el.style.color = '#8B8B8B';
    el.style.fontSize = '14px'
  }
});
Vue.directive('darkBackground', {
  inserted: function (el) {
    el.style.background = '#282D30';
  }
});
Vue.directive('animate', {
  inserted: function (el) {
    el.style.transition = '0.3s linear';
  }
});

/*css*/
import 'vue-select/dist/vue-select.css';

new Vue({
  store,
  axios,
  render: h => h(App),
}).$mount('#app')
