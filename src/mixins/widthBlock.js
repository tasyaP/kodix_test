export default {
    methods: {
        widthBlockName() {
            let widthHeadName = this.$el.querySelector('.table__header_name').clientWidth;
            let nameAndDescElements = this.$el.querySelectorAll('.table__row .name, .table__row .desc');

            nameAndDescElements.forEach(element => element.style.width = widthHeadName + 'px');
        }
    }
}